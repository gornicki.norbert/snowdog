import '../css/app.css'

let baconImage = document.querySelector('img[alt="Beautiful slices of fried bacon"]')
if (baconImage) {
  let section = baconImage.parentNode
  document.querySelector('button[type="button"]').addEventListener('click', function () {
    section.innerHTML = section.innerHTML + baconImage.outerHTML
  })
}

let orderForm = document.getElementById('orderForm')
if (orderForm) {
  orderForm.addEventListener('submit', function (e){
    e.preventDefault()
    let formData = new FormData()
    let inputs = this.querySelectorAll('input')
    for(let input of inputs) {
      if (input.name) {
        formData.append(input.name, input.value)
      }
    }
    let selects = this.querySelectorAll('select')
    for(let select of selects) {
      if (select.name) {
        formData.append(select.name, select.value)
      }
    }

    fetch('http://localhost:3333/order', {
      method: 'POST',
      body: formData,
    }).then(function (response) {
      response.json().then(function (data) {
        document.querySelectorAll('.form__input--error').forEach(e => e.remove())
        if (response.status === 200) {
          let button = orderForm.querySelector('button')
          button.setAttribute('disabled', 'true')
          button.innerText = data.message
        } else {
          for (let fieldName in data) {
            let fieldParent = orderForm.querySelector('[name="' + fieldName + '"]').parentNode
            fieldParent.innerHTML = fieldParent.innerHTML + '<div class="form__input--error">' + data[fieldName] + '</div>'
          }
        }
      })
    })
  })
}
